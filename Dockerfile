FROM node:16-alpine as builder
## /app folder del container
WORKDIR '/app'
COPY package.json .
RUN npm i
COPY . .
RUN npm run build

FROM nginx
COPY --from=builder /app/build /usr/share/nginx/html
# run
# docker build .
# docker run -p 80:80 929d762